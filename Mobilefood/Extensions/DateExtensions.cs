﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilefood.Extensions
{
    public static class DateExtensions
    {

        public static int GetWeekOfYear(this DateTime date)
        {
            Calendar cal = DateTimeFormatInfo.CurrentInfo.Calendar;
            return cal.GetWeekOfYear(
                date,
                CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek
                );
        }
    }
}
