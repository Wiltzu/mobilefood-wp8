﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilefood.Utils
{
    public interface INavigationService
    {
        void Navigate<T>(int panoramaItemNumber);
    }
}
