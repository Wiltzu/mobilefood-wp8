using System;
using Microsoft.Phone.Controls;

namespace Mobilefood.Utils
{
    public class NavigationService : INavigationService
    {
        public PhoneApplicationFrame RootFrame { get; set; }

        public NavigationService(PhoneApplicationFrame rootFrame)
        {
            RootFrame = rootFrame;
        }

        public void Navigate<T>(int panoramaItemNumber)
        {
            var type = typeof(T);

            var page = (type.FullName.Substring(type.FullName.IndexOf('.')).Replace('.', '/')) + ".xaml";

            RootFrame.Navigate(new Uri(string.Format("{0}?goto={1}", page, panoramaItemNumber), UriKind.Relative));
        }
    }
}
