﻿using System.Linq;
using System.Windows.Navigation;
using GalaSoft.MvvmLight;
using Mobilefood.Extensions;
using Mobilefood.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Mobilefood.Utils;
using RestSharp.Extensions;

namespace Mobilefood.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IFoodService _foodService;
        private readonly INavigationService _navigationService;
        private bool _isLoadingFoods;
        private string _panoramaTitle;

        private int DayOfTheWeek
        {
            get
            {
                var today = SystemTime.Now().Date;
                var numberStartingFromSunday = (int)today.DayOfWeek;
                return numberStartingFromSunday == 0 ? 6 : numberStartingFromSunday - 1;
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IFoodService foodService, INavigationService navigationService)
        {
            _foodService = foodService;
            _navigationService = navigationService;
            FoodsByWeekDay = new ObservableCollection<WeekDay>();

            var today = SystemTime.Now().Date;
            var weekNumber = today.GetWeekOfYear();
            var year = today.Year;

            PanoramaTitle = "Viikon " + weekNumber + " opiskelijalounaat";
            IsLoadingFoods = true;
            _foodService.GetFoods(weekNumber, year, FoodsChanged);
        }

        public void FoodsChanged(IList<WeekDay> foodsByWeekDays)
        {
            IsLoadingFoods = false;
            FoodsByWeekDay.Clear();
            foodsByWeekDays.ToList().ForEach(FoodsByWeekDay.Add);
            // TODO: Figure out a right place to do this!!
            // _navigationService.Navigate<MainPage>(DayOfTheWeek);
        }

        public ObservableCollection<WeekDay> FoodsByWeekDay
        {
            get;
            private set;
        }

        public bool IsLoadingFoods
        {
            get { return _isLoadingFoods; }
            private set
            {
                _isLoadingFoods = value;
                RaisePropertyChanged("IsLoadingFoods");
            }
        }
        public string PanoramaTitle
        {
            get { return _panoramaTitle; }
            private set
            {
                _panoramaTitle = value;
                RaisePropertyChanged("PanoramaTitle");
            }
        }

    }
}