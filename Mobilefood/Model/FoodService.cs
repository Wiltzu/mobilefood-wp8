using Mobilefood.Model;
using Mobilefood.Model.Integrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Mobilefood.Model
{

    public class FoodService : IFoodService
    {
        private readonly IFoodIntegration _integration;

        public FoodService(IFoodIntegration integration)
        {
            _integration = integration;
        }

        public async void GetFoods(int weekNumber, int year, Action<IList<WeekDay>> callback)
        {
            var dto = await _integration.GetFoods(weekNumber, year);

           mapLunchTimes(dto.FoodsByDay, dto.Restaurants);

            callback(dto.FoodsByDay);
        }

        private void mapLunchTimes(List<WeekDay> foodsByDay, List<Restaurant> restaurants)
        {
            foreach (var weekDay in foodsByDay)
            {
                for (var i = 0; i < weekDay.FoodsByRestaurant.Count; i++)
                {
                    RestaurantDay restaurantDay = weekDay.FoodsByRestaurant[i];
                    Restaurant restaurant = restaurants.Find((r) => r.Name.Equals(restaurantDay.RestaurantName));
                    
                    var lunchTime = restaurant.LunchTimes.Find((time) => time.WeekDays.Contains(WEEK_DAYS[weekDay.Day]));
                    if(lunchTime != null) restaurantDay.LunchTime = lunchTime.Hours;
                }
            }
        }

        private static String[] WEEK_DAYS = {"ma", "ti", "ke", "to", "pe", "la", "su"}; 
    }
}
