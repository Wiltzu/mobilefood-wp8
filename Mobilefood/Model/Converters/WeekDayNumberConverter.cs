﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Mobilefood.Model.Converters
{
    public class WeekDayNumberConverter : IValueConverter
    {
        private readonly Dictionary<int, string> _numberToWeekDay = new Dictionary<int, string>()
        {
            {0, "Maanantai"},
            {1, "Tiistai"},
            {2, "Keskiviikko"},
            {3, "Torstai"},
            {4, "Perjantai"},
            {5, "Lauantai"},
            {6, "Sunnuntai"}
        }; 

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var weekDayNumber = (int) value;
            return _numberToWeekDay[weekDayNumber];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}