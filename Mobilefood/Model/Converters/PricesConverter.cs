﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Mobilefood.Model.Converters
{
    public class PricesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var prices = (IList<String>) value;
            return string.Join(" / ", prices);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
