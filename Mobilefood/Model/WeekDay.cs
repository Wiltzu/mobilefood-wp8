﻿using System.Collections.Generic;
using System.Windows.Controls.Primitives;

namespace Mobilefood.Model
{
    public class WeekDay
    {
        public int Day { get; set; }
        public List<RestaurantDay> FoodsByRestaurant { get; set; }
    }
}
