﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobilefood.Model
{
    /// <summary>
    /// This class represents Restaurant's lunch times.
    /// </summary>
    public class LunchTime
    {

        public LunchTime(string hours, string weekDays)
        {
            Hours = hours;
            WeekDays = weekDays;
        }

        public LunchTime() { }

        public string Hours
        {
            get;
            private set;
        }

        public string WeekDays
        {
            get;
            private set;
        }
    }
}
