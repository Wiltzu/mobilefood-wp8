﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilefood.Model
{
    /// <summary>
    /// This class represents single food in Restaurant.
    /// </summary>
    public class Food
    {
        public string Name
        {
            get;
            set;
        }

        public List<string> Prices
        {
            get;
            set;
        }

        public string Diets
        {
            get;
            set;
        }

        public Food(string name, List<string> prices, string diets)
        {
            Name = name;
            Prices = prices;
            Diets = diets;
        }

        public Food() { }

        public override string ToString()
        {
            return string.Format("%s %s", Name + Prices);
        }
    }
}
