﻿using System;
using System.Collections.Generic;
namespace Mobilefood.Model
{
    public interface IFoodService
    {
        /// <summary>
        /// TODO: Consider having Exception in callback also
        /// </summary>
        /// <param name="weekNumber"></param>
        /// <param name="year"></param>
        /// <param name="callback"></param>
        void GetFoods(int weekNumber, int year, Action<IList<WeekDay>> callback);
    }
}
