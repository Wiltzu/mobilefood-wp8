﻿using System;
using System.Threading.Tasks;
namespace Mobilefood.Model.Integrations
{
    public interface IFoodIntegration
    {
        Task<FoodIntegrationDto> GetFoods(int weekNumber, int year);
    }
}
