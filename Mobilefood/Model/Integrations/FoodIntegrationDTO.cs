﻿using System.Collections.Generic;

namespace Mobilefood.Model.Integrations
{
    public class FoodIntegrationDto
    {
        public List<WeekDay> FoodsByDay { get; set; }
        public List<Restaurant> Restaurants { get; set; }
        public string Version { get; set; }
        public string ChainName { get; set; }
        public string Status { get; set; }
    }
}