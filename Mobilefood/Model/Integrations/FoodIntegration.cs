﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Mobilefood.Extensions;

namespace Mobilefood.Model.Integrations
{
    public class FoodIntegration : IFoodIntegration
    {
        private const string RequestUrlFormat = "mobilerest/?restaurant=unica&year={0}&week={1}";
        private const string ServiceLocationBaseUrl = "http://anssikin.eu:4730/";

        public async Task<FoodIntegrationDto> GetFoods(int weekNumber, int year)
        {         
            var client = new RestClient(ServiceLocationBaseUrl);
            var requetUrl = GetRequetUrlWith(weekNumber, year);
            var request = new RestRequest(requetUrl, Method.GET);
            var response = await client.ExecuteTaskAsync<FoodIntegrationDto>(request);
            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }
            return response.Data;
        }

        private static string GetRequetUrlWith(int weekNumber, int year)
        {       
            return string.Format(RequestUrlFormat, year, weekNumber);
        }
    }
}
