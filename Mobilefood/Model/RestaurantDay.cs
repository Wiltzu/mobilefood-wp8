﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilefood.Model
{

    /// <summary>
    /// This class represents one day in restaurant.
    /// </summary>
    public class RestaurantDay
    {
        public RestaurantDay(String restaurantName, List<Food> lunches, String alert)
        {
            RestaurantName = restaurantName;
            Lunches = lunches;
            Alert = alert;
        }

        public RestaurantDay() { }

        public string RestaurantName
        {
            get;
            set;
        }

        public string LunchTime
        {
            get;
            set;
        }

        public List<Food> Lunches
        {
            get;
            set;
        }

        public string Alert
        {
            get;
            set;
        }
    }
}
