﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobilefood.Model
{
    public class Restaurant
    {
        public string Name { 
            get; 
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string Zip
        {
            get;
            set;
        }

        public string PostOffice
        {
            get;
            set;
        }

        public string Longitude
        {
            get;
            set;
        }

        public string Latitude
        {
            get;
            set;
        }

        public List<LunchTime> LunchTimes
        {
            get;
            set;
        } 

    }

}
