using Mobilefood.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobilefood.Design
{

    public class DesignFoodService : IFoodService
    {
        public void GetFoods(int weekNumber, int year, Action<IList<WeekDay>> callback)
        {
            var daysFoods = new List<RestaurantDay>();
            var weekDay = new WeekDay() {Day = 0, FoodsByRestaurant = daysFoods};
            var restaurantDay = new RestaurantDay {RestaurantName = "Assarin Ullakko", LunchTime = "10.30-15.00"};
            daysFoods.Add(restaurantDay);
            
            var foodList = new List<Food>();
            restaurantDay.Lunches = foodList;
            for (int i = 0; i < 10; i++)
            {
                foodList.Add(new Food("Makaroonilaatikkooooo ooooooooooooooo ooooooooooooooo" + i, new List<string>() { "2,60", "5,40", "7,20" }, "G L M"));
            }
            callback(new List<WeekDay>() {weekDay});
        }
    }
}
