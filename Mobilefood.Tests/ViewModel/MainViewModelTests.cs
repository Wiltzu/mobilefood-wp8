﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using Mobilefood.Model;
using Mobilefood.Utils;
using Mobilefood.ViewModel;
using NUnit.Framework;
using PaulWade.MVVMlightNavigationService;

namespace Mobilefood.Tests.ViewModel
{
    [TestFixture]
    public class MainViewModelTests
    {
        private IFoodService _foodService;
        private INavigationService _navigationService;

        [SetUp]
        public void SetUp()
        {
            _foodService = A.Fake<IFoodService>();
            _navigationService = A.Fake<INavigationService>();
        }

        [TearDown]
        public void TearDown()
        {
            SystemTime.ResetDateTime();
        }

        [Test]
        public void MainViewModel_Always_GetsFoodFromServiceForCurrentWeekAndYear()
        {
            const int expectedWeekNumber = 36;
            const int expectedYear = 2014;
            SystemTime.SetDateTime(new DateTime(expectedYear, 9, 1));

            var mainViewModel = new MainViewModel(_foodService, _navigationService);

            A.CallTo(() => _foodService.GetFoods(expectedWeekNumber, expectedYear, A<Action<IList<WeekDay>>>._)).MustHaveHappened();
        }
    }
}
