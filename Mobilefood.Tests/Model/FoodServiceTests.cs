﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using Mobilefood.Model;
using Mobilefood.Model.Integrations;

namespace Mobilefood.Tests.Model
{
    [TestFixture]
    class FoodServiceTests
    {
        private IFoodService _foodService;
        private IFoodIntegration _foodIntegrationFake;
        private Action<IList<WeekDay>> _callbackFake;
        private FoodIntegrationDto _defaultDto;

        [SetUp]
        public void SetUp()
        {
            _foodIntegrationFake = A.Fake<IFoodIntegration>();
            _callbackFake = A.Fake<Action<IList<WeekDay>>>();
            _foodService = new FoodService(_foodIntegrationFake);

            ReturnDefaultDtoFromIntegration();
        }

        private void ReturnDefaultDtoFromIntegration()
        {
            _defaultDto = new FoodIntegrationDto
            {
                FoodsByDay = new List<WeekDay>()
                {
                    new WeekDay()
                    {
                        FoodsByRestaurant = new List<RestaurantDay>()
                    }
                }
            };
            A.CallTo(() => _foodIntegrationFake.GetFoods(0, 0)).Returns(Task.FromResult(_defaultDto));
        }

        [Test]
        public void GetFoods_Always_CallsIntegrationWithCorrectParams()
        {
            _foodService.GetFoods(1, 2014, _callbackFake);

            A.CallTo(() => _foodIntegrationFake.GetFoods(1, 2014)).MustHaveHappened();
        }

        [Test]
        public void GetFoods_Always_CallsCallbackWithIntegrationResult()
        {
            ReturnDefaultDtoFromIntegration();

            _foodService.GetFoods(1, 2014, _callbackFake);

            A.CallTo(() => _callbackFake(A<List<WeekDay>>._)).MustHaveHappened();
        }
    }
}


