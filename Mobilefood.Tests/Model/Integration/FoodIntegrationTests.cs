﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using Mobilefood.Model.Integrations;
using Mobilefood.Tests.Model.Integration;

namespace Mobilefood.Tests.Model.Integration
{
    [TestFixture]
    class FoodIntegrationTests
    {
        private FoodIntegration _foodIntegration;

        [SetUp]
        public void SetUp()
        {
            _foodIntegration = new FoodIntegration();
        }

        [Test]
        public async void GetFoods_Always_ReturnsNonEmptyListOfFoods()
        {
           var response = await _foodIntegration.GetFoods(0, 0);
           Assert.AreEqual(null, response);
        }
    }
}                          